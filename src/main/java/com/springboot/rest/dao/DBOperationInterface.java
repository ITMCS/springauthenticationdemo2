package com.springboot.rest.dao;

import java.math.BigInteger;
import java.util.List;

import com.springboot.rest.entity.ConfirmationTokenModel;
import com.springboot.rest.entity.UserModel;

/**
 * SpringBootRestDemo 2019 Filename: DBOperationInterface.java Description: Repository Interface
 * request and response to appropriate format,
 *
 * @author Itmusketeers
 * @version 1.0
 * @Last modified 2019-03-07
 */
public interface DBOperationInterface {

	/* Used for get all User form database */
	List<UserModel> getAllUsers();
	
	/* Used for insert User form database */
	void addUser(UserModel user);
	
	/* Used for update User form database */
    void updateUser(UserModel user);
    
    /* Used for delete token entry form database according to user id*/
    void deleteConfirmToken(BigInteger userID);
    
    /* Used for check user is exist in database or not */
	boolean userExists(UserModel user);
	
	/* Used for check token is exist in database or not */
	boolean tokenExists(String token);
	
	/* Used for fetch user by userName */
	UserModel fetchUserbyUserName(String userName);
	
	
	/* Used for insert a confirmation token */
	ConfirmationTokenModel addConfirmationToken(ConfirmationTokenModel confirmationTokenModel);
	
	/* Used for fetch ConfirmationTokenModel by token */
	ConfirmationTokenModel fetchConfirmTokenModelbyToken(String token);
}
