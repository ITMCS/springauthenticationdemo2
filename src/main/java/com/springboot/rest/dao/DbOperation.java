package com.springboot.rest.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.rest.entity.ConfirmationTokenModel;
import com.springboot.rest.entity.ConfirmationTokenRowMapper;
import com.springboot.rest.entity.UserModel;
import com.springboot.rest.entity.UserRowMapper;


/**
 * SpringBootRestDemo 2019 Filename: DbOperation.java Description: Repository class
 * request and response to appropriate format,
 *
 * @author Itmusketeers
 * @version 1.0
 * @Last modified 2019-03-07
 */
@Transactional
@Repository
public class DbOperation implements DBOperationInterface{

	private final JdbcTemplate jdbcTemplate;
	 
	@Autowired
	public DbOperation(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
     *  fetch all user 
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@Override
	public List<UserModel> getAllUsers() {
		String sql = "SELECT userpk,firstname, lastname, username, password, isverify FROM tbl_users";	
		RowMapper<UserModel> rowMapper = new UserRowMapper();
		return this.jdbcTemplate.query(sql, rowMapper);
	}
	
	/**
     * fetch user by username and pass user name as parameter
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@Override
	public UserModel fetchUserbyUserName(String userName) {
		String sql = "SELECT userpk,firstname, lastname, username, password, isverify FROM tbl_users WHERE username = ?";
		RowMapper<UserModel> rowMapper = new UserRowMapper();
		UserModel userModel = jdbcTemplate.queryForObject(sql, rowMapper, userName);
		return userModel;
		
	}

	/**
     * add user mehtod and User model used as a parameter where some important details are required,
     * like firstname, lastname, username, password.
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@Override
	public void addUser(UserModel user) {
		String sql = "INSERT INTO tbl_users (firstname, lastname, username, password, isverify) values (?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, user.getFirstName(),user.getLastName(),user.getUserName(),user.getPassword(),user.getIsVerify());
		
	}
	

	/**
     * Update the user, here identify column is update  after email verification
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@Override
	public void updateUser(UserModel user) {		
		String sql = "UPDATE tbl_users SET isverify=? WHERE userpk=?";
		jdbcTemplate.update(sql, 1, user.getUserId());		
	}
	
	
	/**
     * Check is User is available in database  by username 
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@Override
	public boolean userExists(UserModel user) {
		String sql = "SELECT count(*) FROM tbl_users WHERE username = ? ";
		int count = jdbcTemplate.queryForObject(sql, Integer.class, user.getUserName());
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
     * Check is token is available in database 
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@Override
	public boolean tokenExists(String token) {
		String sql = "SELECT count(*) FROM tbl_confirmationtoken WHERE confirmationtoken = ? ";
		int count = jdbcTemplate.queryForObject(sql, Integer.class, token);
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
     * Insert token in the database where cretaeddate and user id is important  
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@Override
	public ConfirmationTokenModel addConfirmationToken(ConfirmationTokenModel confirmationTokenModel) {
		String sql = "INSERT INTO tbl_confirmationtoken (confirmationtoken, createdDate, userid) values (?, ?, ?)";
		jdbcTemplate.update(sql, confirmationTokenModel.getConfirmationToken(),new Date(),confirmationTokenModel.getUser());
		return confirmationTokenModel;
	}

	/**
     * Fetch token model form database according to token
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@Override
	public ConfirmationTokenModel fetchConfirmTokenModelbyToken(String token) {
		String sql = "SELECT confirmationtokenPk,confirmationtoken,createdDate,userid FROM tbl_confirmationtoken WHERE confirmationtoken = ?";
		RowMapper<ConfirmationTokenModel> rowMapper = new ConfirmationTokenRowMapper();
		ConfirmationTokenModel confirmationTokenModel = jdbcTemplate.queryForObject(sql, rowMapper, token);
		return confirmationTokenModel;
	}

	/**
     * Delete confirm token after successful email authentication.
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@Override
	public void deleteConfirmToken(BigInteger userID) {
		String sql = "DELETE FROM tbl_confirmationtoken WHERE userid=?";
		jdbcTemplate.update(sql, userID);		
	}

}
